# Aplikace pro evidenci geometrických plánů

* aplikace je napsaná v Pythonu
* využívá framework Flask pro HTTP přístup a integraci do webových serverů
* pro perzistenci objektů do relační DB (ORM) používá knihovnu PeeWee
* jako relační databázi vyžaduje PostgreSQL
* pro správu se používá skript _management.py_ přístupný pres SSH jako shell
* je určena pro integraci do Lighttpd serveru přes FastCGI
* zveřejněna pod GPL licencí

## Popis souborů a adresářů

### Adresář _deb_ - adresář pro vytvoření instalačního DEB balíčku 

* deb/Makefile - hlavní cíl _egp.deb_ vytvoří instalační balíček
* deb/postinst - skript automaticky spuštěný po instalaci balíčku - vytvoří systémového uživatele _egp_ a nastaví práva adresářům ve _/var_
* deb/postrm - skript automaticky spuštěný po odinstalování balíčku - odstraní uživatele _egp_
* deb/control - metadata balíčku
* deb/copyright - GPL licence

### Adresář _egp_ - obashuje samotný kód aplikace

* egp/config.py - konfigurace aplikace
* egp/management.py - skript pro správu aplikace používaný jako shell uživatele _egp_
* egp/auth.py - autentizační modul
* egp/authdigest.py - pomocný modul pro Digest HTTP autentizaci
* egp/render.py - pomocné třídy pro rendering do HTTP
* egp/i18n.py - internacionalizační řetězce
* egp/log.py - logování do syslogu
* egp/model.py - definice datových objektů pro PeeWee ORM
* egp/\_\_init\_\_.py
* egp/view.py - definice HTTP formulářů
* egp/gmail.py - odesílání emailových zpráv přes GMail účet (použito pro zálohování)

### Ostatní

* egp.fcgi - spouštěcí skript FastCGI procesu používaný Lighttpd
* README.txt - tento soubor
* 99-egp.conf - konfigurační soubor pro Lighttpd pro zveřejnění aplikace _egp_

## Instalace

Nainstalovat prerekvizity:

	# apt-get install python lighttpd postgresql python-flask python-psycopg2 python-flup
	
Nainstalovat peewee:

	# apt-get install python-pip
	# pip install peewee
	
Stáhnout a nainstalovat egp.deb balíček:

	# dpkg -i egp.deb

Nastavit uživateli "egp" heslo:

	# passwd egp
	
Vytvořit databáze a databázového uživatele:

	$ sudo -u postgres psql
	postgres=# create user egp;
	postgres=# create database egp with owner egp;
	postgres=# create database ing with owner egp;
	postgres=# ^D
	
Zrušit lokální autentizaci uživatele egp:

	# vi /etc/postgresql/*/main/pg_hba.conf
	Nahoru přidat řádky:
	local	egp	egp	trust
	local	ing	egp	trust

Vytvořit obsah databáze:

	$ ssh egp@localhost
	> createdb

## Obnova databáze z emailové zálohy

Dekomprimovat zálohový soubor z emailu:

	# gunzip pgsql_backup.gz

Smazat obsah databáze (odstranit a vytvořit prázdnou):

	$ sudo -u postgres psql
	postgres=# drop database egp;
	postgres=# create database egp with owner egp;

Restaurovat obsah databáze ze zálohy:

	# psql -U egp -f pgsql_backup egp

