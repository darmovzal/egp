import config

def _href(url):
	return config.ROOT_URL_PATH % url if url != None else None

class Renderer(object):
	def __init__(self):
		self.data = []
	
	def append(self, *args):
		self.data.extend(args)
		return self
	
	def nl(self):
		return self.append('\n')
	
	def as_string(self):
		return ''.join(map(unicode, self.data))
	
class HtmlRenderer(Renderer):
	def escape(self, value):
		if value is None: return self
		value = unicode(value)
		value = value.replace('&', '&amp;')
		value = value.replace('"', '&quot;')
		value = value.replace("'", '&apos;')
		value = value.replace('<', '&lt;')
		value = value.replace('>', '&gt;')
		return self.append(value)
	
	def atts(self, values):
		if not values: return self
		if len(values) % 2: raise Exception('Odd number of attributes')
		for i in xrange(len(values) / 2):
			aname = values[i * 2]
			avalue = values[i * 2 + 1]
			if avalue == None: continue
			self.append(' ', aname, '="').escape(avalue).append('"')
		return self
	
	def begin(self, tag, *atts):
		return self.append('<', tag).atts(atts).append('>')
	
	def empty(self, tag, *atts):
		return self.append('<', tag).atts(atts).append('/>')
	
	def end(self, tag):
		return self.append('</', tag, '>')

	def css(self, selector, *args):
		if len(args) % 2: raise Exception('Odd CSS parameters count')
		self.append(selector, ' { ')
		for i in xrange(len(args) / 2):
			self.append(args[i * 2], ': ', args[i * 2 + 1], '; ')
		return self.append('}').nl()
	
	def a(self, href, text):
		return self.begin('a', 'href', _href(href)).escape(text).end('a')
	
	def comment(self, text):
		return self.append('<!-- ', text, ' -->').nl()

class GuiRenderer(HtmlRenderer):
	def style_screen(self):
		self.css('*', 'font-family', 'Garamond, Georgia, serif')
		self.css('#top', 'width', '80%', 'margin', '70px auto')
		self.css('a', 'color', 'inherit')
		self.css('.table', 'width', '100%', 'border-top', '2px solid black', 'border-bottom', '1px solid black')
		self.css('.table th, .table td', 'text-align', 'left', 'vertical-align', 'top', 'padding', '2px 10px')
		self.css('.table .header th', 'background-color', '#f8f8f8')
		self.css('.table .first-sep td', 'border-top', '1px solid black')
		self.css('.table .pages td', 'border-top', '1px solid black', 'background-color', '#f8f8f8')
		self.css('.table .pages td div', 'overflow-y', 'auto', 'line-height', '1.5em', 'height', '1.5em')
		self.css('.table .sep td', 'border-top', '1px solid #eee')
		self.css('.table .odd td, .table .odd th', 'background-color', '#f8f8f8')
		self.css('.table .group-sep td', 'border-top', '1px solid black')
		self.css('.table ul', 'margin', '0', 'padding-left', '20px')
		self.css('.table ul li', 'list-style-type', 'square')
		self.css('#menu', 'background-color', '#444', 'color', '#eee', 'padding', '5px 30px', 'position', 'fixed', 'left', 0, 'top', 0, 'width', '100%', 'font-size' ,'90%')
		self.css('#menu a', 'text-decoration', 'none')
		self.css('.form', 'width', '100%', 'border-top', '2px solid black', 'border-bottom', '1px solid black', 'background-color', '#f8f8f8')
		self.css('.form .text, .form textarea', 'width', '100%', 'border', '1px solid #aaa')
		self.css('.form textarea', 'height', '100px')
		self.css('.form td, .form th', 'text-align', 'left', 'vertical-align', 'top', 'padding', '2px 10px')
		self.css('.form th', 'width', '20%')
		self.css('.form .button', 'border', '1px solid #aaa', 'padding', '5px 20px', 'background-color', 'white')
		self.css('.form .check', 'border', '1px solid #aaa')
		self.css('.dialog', 'border-top', '2px solid black', 'border-bottom', '1px solid black', 'background', '#f8f8f8', 'text-align', 'center', 'padding', '20px 50px')
		self.css('.hilite', 'color', '#bb0000')
		return self

	def style_print(self):
		self.css('#menu, h1, h2, h3', 'display', 'none')
		self.css('*', 'font-family', 'Garamond, Georgia, serif')
		self.css('.table', 'width', '100%', 'border-top', '2px solid black', 'border-bottom', '1px solid black')
		self.css('.table th, .table td', 'text-align', 'left', 'vertical-align', 'top', 'padding', '2px 10px')
		self.css('.table .first-sep td, .table .sep td', 'border-top', '1px solid black')
		self.css('.table .group-sep td', 'border-top', '2px solid black')
		self.css('.table .pages td', 'display', 'none')
		self.css('.nowrap', 'white-space', 'nowrap')
		self.css('.table ul', 'margin', '0', 'padding-left', '20px')
		self.css('.table ul li', 'list-style-type', 'square')
		self.css('a', 'text-decoration', 'none', 'color', 'black')
		self.css('.hilite', 'color', '#bb0000')
		return self

	def page_begin(self, title):
		self.begin('html').nl()
		self.begin('head').nl()
		self.begin('title').escape(title).end('title').nl()
		self.begin('style', 'media', 'screen').nl().style_screen().end('style').nl()
		self.begin('style', 'media', 'print').nl().style_print().end('style').nl()
		self.script("""
			function is_checked(fname, ename){
				var values = document.forms[fname].elements[ename];
				for(var i = 0; i < values.length; i++){
					if(values[i].checked) return true;
				}
				return false;
			}
			function is_filled(fname, ename){
				var e = document.forms[fname].elements[ename];
				return e.value.trim().length > 0;
			}
		""")
		self.end('head').nl()
		self.begin('body').nl()
		return self.begin('div', 'id', 'top').nl()
	
	def page_end(self):
		return self.end('div').nl().end('body').nl().end('html').nl()
	
	def tdb(self, colspan = None, rowspan = None, clazz = None):
		return self.begin('td', 'rowspan', rowspan, 'colspan', colspan, 'class', clazz)

	def tde(self):
		return self.end('td').nl()

	def th(self, text, colspan = None, rowspan = None):
		return self.begin('th', 'rowspan', rowspan, 'colspan', colspan).escape(text).end('th').nl()

	def table_begin(self):
		return self.begin('table', 'class', 'table', 'cellspacing', 0).nl()
	
	def table_end(self):
		return self.end('table').nl()

	def table_pages(self, count, page_len, current, format, columns):
		if count == 0: return
		pages = count / page_len
		if count % page_len: pages += 1
		if pages == 1: return
		self.row_begin('pages').tdb(columns).begin('div')
		for p in xrange(pages):
			if p: self.append(' - ')
			if p + 1 == current:
				self.append(p + 1)
			else:
				self.a(format % (p + 1), p + 1)
		return self.end('div').tde().row_end()
	
	def row_begin(self, *classes):
		clazz = ' '.join(c if c else '' for c in classes)
		return self.begin('tr', 'class', clazz).nl()

	def row_end(self):
		return self.end('tr').nl()

	def ul_begin(self):
		return self.begin('ul').nl()

	def li(self, text, clazz = None):
		return self.begin('li', 'class', clazz).escape(text).end('li').nl()
	
	def ul_end(self):
		return self.end('ul').nl()

	def menu(self, items):
		self.begin('div', 'id', 'menu').nl()
		for i in xrange(len(items)):
			if i: self.append(' | ')
			self.a(items[i][0], items[i][1]).nl()
		return self.end('div').nl()

	def form_begin(self, path, onsubmit = None):
		self.begin('form', 'method', 'post', 'action', _href(path), 'onsubmit', onsubmit).nl()
		return self.begin('table', 'class', 'form', 'cellspacing', 0)
	
	def form_end(self):
		return self.end('table').nl().end('form').nl()

	def form_text(self, value):
		return self.tdb().escape(value).tde()
	
	def form_edit(self, name, value = None):
		return self.tdb().empty('input', 'type', 'text', 'name', name, 'value', value, 'class', 'text').tde()
	
	def form_area(self, name, value = None):
		return self.tdb().begin('textarea', 'name', name).escape(value).end('textarea').tde()

	def form_check(self, name, value, title, checked = False, clazz = None):
		return self.empty('input', 'type', 'checkbox', 'name', name, 'value', value, 'class', 'check', 'checked',
			'checked' if checked else None).begin('span', 'class', clazz).escape(title).end('span').empty('br').nl()
	
	def form_submit(self, title):
		return self.empty('input', 'type', 'submit', 'value', title, 'class', 'button').nl()
	
	def header(self, title, level = 1):
		return self.begin('h%d' % level).escape(title).end('h%d' % level).nl()
	
	def dialog_begin(self):
		return self.begin('div', 'class', 'dialog').nl()
	
	def dialog_end(self):
		return self.end('div').nl()
	
	def link(self, href, text):
		return self.begin('span', 'class', 'link').a(href, u'>>> %s' % text).end('span').empty('br').nl()

	def script(self, text):
		return self.begin('script', 'type', 'text/javascript').nl().append(text).nl().end('script').nl()

class CsvRenderer(Renderer):
	def __init__(self):
		super(CsvRenderer, self).__init__()
		self.newrow = True
	
	def item(self, value):
		if self.newrow:
			self.newrow = False
		else:
			self.append(',')
		self.append('"', unicode(value).replace('"', "'"), '"')
		return self
	
	def end(self):
		self.newrow = True
		return self.append('\r\n')
	
	def row(self, *items):
		for item in items:
			self.item(item)
		return self.end()

