import config

if __name__ == "__main__":
	import sys
	if len(sys.argv) == 1:
		print 'Usage: python %s <db_id> ...' % sys.argv[0]
		exit(1)
	config.set_db(sys.argv[1])

from flask import Flask, request, Response
import render
from i18n import R
import model
from datetime import datetime
import auth
from log import access_log
from functools import wraps

app = Flask(__name__)

def logged_user():
	return model.Uzivatel.by_login(request.authorization.username)

def is_admin(u):
	return u.opravneni == model.Uzivatel.OPRAVNENI_ADMINISTRATOR

def render_menu(r, uzivatel):
	admin = is_admin(uzivatel)
	menu = [
		(None, '[%s]' % config.DB_ID),
		(None, R('prihlasen-x') % (uzivatel.jmeno, ' (admin)' if admin else '')),
		('/records/1', R('zaznamy')),
		('/record/new', R('novy-zaznam')),
		('/history/1', R('historie')),
	]
	if admin:
		menu.append(('/list-users', R('seznam-uzivatelu')))
		menu.append(('/export', R('export')))
	r.menu(menu)

def export(name):
	def decorator2(f):
		@wraps(f)
		def decorator(*args, **kwargs):
			r = render.CsvRenderer()
			f(r, *args, **kwargs)
			filename = '%s_%s.csv' % (name, format(datetime.now(), '%Y-%m-%d_%H-%M-%S'))
			res = Response(r.as_string(), mimetype='application/vnd.ms-excel')
			res.headers['Content-Disposition'] = 'attachment;filename=%s' % filename
			return res
		return decorator
	return decorator2

@app.after_request
def suppress_caching(response):
	#response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
	#response.headers['Cache-Control'] = 'public, max-age=0'
	response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
	response.headers['Pragma'] = 'no-cache'
	response.headers['Expires'] = '0'
	return response

@app.route("/")
@app.route('/records/<int:page>')
@auth.requires_auth
@access_log
def records(page = 1):
	uzivatel = logged_user()
	uzivatele = {}
	for u in model.Uzivatel.select():
		uzivatele[u.login] = u.jmeno
	
	r = render.GuiRenderer()
	r.page_begin(R('titulek'))
	render_menu(r, uzivatel)

	r.header(R('zaznamy'))
	r.table_begin()
	r.row_begin('header')
	r.th(R('cislo')).th(R('rok')).th(R('datum')).th(R('cas')).th(R('katastralni-uzemi')).th(R('cislo-zakazky')).th(R('kdo'))
	r.row_end()

	PAGE_LEN = 10
	i = 0
	last_group = None
	for zaznam in model.Zaznam.select_authorized(uzivatel).order_by(model.Zaznam.vytvoren.desc()).paginate(page, PAGE_LEN):
		#if not zaznam.is_authorized(uzivatel): continue
		odd = 'odd' if i % 2 else None
		poznamka = zaznam.poznamka
		group = (zaznam.vytvoren.year, zaznam.vytvoren.month)
		r.row_begin('sep' if i else 'first-sep', odd, 'group-sep' if last_group and (last_group != group) else None)
		last_group = group
		r.tdb(1, 3 if poznamka else 2)
		r.a('/record/%d' % zaznam.id, zaznam.format_cislo())
		r.tde()
		r.tdb().append(zaznam.vytvoren.year).tde()
		r.tdb().append(zaznam.vytvoren.strftime('%d.%m.')).tde()
		r.tdb().append(zaznam.vytvoren.strftime('%H:%M')).tde()
		r.tdb(clazz = 'nowrap').escape(zaznam.katastralni_uzemi).tde()
		r.tdb(clazz = 'nowrap').escape(zaznam.cislo_zakazky).tde()
		r.tdb(clazz = 'nowrap').escape(uzivatele[zaznam.zadavatel] if zaznam.zadavatel in uzivatele else zaznam.zadavatel).tde()
		r.row_end()
		r.row_begin(odd)
		r.th(R('ucel'), 2)
		r.tdb(4).ul_begin()
		for uz in model.UcelZaznamu.select().where(model.UcelZaznamu.zaznam == zaznam):
			r.li(uz.ucel.nazev, 'hilite' if uz.ucel.hilite else None)
		r.ul_end().tde()
		r.row_end()
		if poznamka:
			r.row_begin(odd)
			r.th(R('poznamka'), 2)
			r.tdb(4).begin('pre').escape(poznamka).end('pre').tde()
			r.row_end()
		i += 1
	if not i:
		r.row_begin('first-sep').tdb(7).append(R('seznam-je-prazdny')).tde().row_end()
	
	r.table_pages(model.Zaznam.select_authorized(uzivatel).count(), PAGE_LEN, page, '/records/%d', 7)
	r.table_end()
	r.page_end()
	return r.as_string()

@app.route("/record/<id>")
@auth.requires_auth
@access_log
def record(id):
	uzivatel = logged_user()
	if id == 'new':
		zaznam = None
	else:
		zaznam = model.Zaznam.select().where(model.Zaznam.id == int(id)).get()
		assert zaznam.is_authorized(uzivatel)
	
	r = render.GuiRenderer()
	r.script(r'''
		function check_form(){
			var msg = '';
			if(!is_filled(0, 'katastralni_uzemi')) msg += '%s\n';
			if(!is_filled(0, 'cislo_zakazky')) msg += '%s\n';
			if(!is_checked(0, 'ucel')) msg += '%s\n';
			if(msg.length == 0) return true;
			alert(msg);
			return false;
		}
	''' % (
		R('err_empty_katastralni_uzemi'),
		R('err_empty_cislo_zakazky'),
		R('err_empty_ucel'),
	))
	r.page_begin(R('titulek'))
	render_menu(r, uzivatel)
	
	r.header(R('editace-zaznamu') if zaznam else R('novy-zaznam'))
	r.form_begin('/save/%s' % id, 'return check_form();')
	if zaznam and zaznam.cislo:
		r.row_begin().th(R('cislo')).form_text(zaznam.format_cislo()).row_end()
	if zaznam and zaznam.vytvoren:
		r.row_begin().th(R('vytvoren')).form_text(format(zaznam.vytvoren, '%d.%m.%Y %H:%M:%S')).row_end()
	if zaznam and zaznam.zadavatel:
		try:
			zadavatel = model.Uzivatel.by_login(zaznam.zadavatel).jmeno
		except:
			zadavatel = zaznam.zadavatel
		r.row_begin().th(R('zadavatel')).form_text(zadavatel).row_end()
	r.row_begin().th(R('katastralni-uzemi')).form_edit('katastralni_uzemi', zaznam.katastralni_uzemi if zaznam else None).row_end()
	r.row_begin().th(R('cislo-zakazky')).form_edit('cislo_zakazky', zaznam.cislo_zakazky if zaznam else None).row_end()
	r.row_begin().th(R('ucel')).tdb()
	ucel_ids = [ u.id for u in zaznam.iter_ucel() ] if zaznam else []
	for ucel in model.Ucel.select().where(model.Ucel.povolen == True).order_by(model.Ucel.id):
		r.form_check('ucel', ucel.id, ucel.nazev, ucel.id in ucel_ids, 'hilite' if ucel.hilite else None)
	r.tde().row_end()
	r.row_begin().th(R('poznamka')).form_area('poznamka', zaznam.poznamka if zaznam else None).row_end()
	r.row_begin().tdb(2).form_submit(R('ulozit')).tde()
	r.form_end()
	
	r.page_end()
	return r.as_string()

@app.route("/save/<id>", methods = ['POST'])
@auth.requires_auth
@access_log
def save(id):
	uzivatel = logged_user()
	
	ku = request.form['katastralni_uzemi'].strip()
	cz = request.form['cislo_zakazky'].strip()
	poznamka = request.form['poznamka'].strip()
	ucely = map(int, request.form.getlist('ucel'))
	
	if not (ku and cz and ucely):
		r = render.GuiRenderer().page_begin(R('titulek'))
		r.dialog_begin().escape('Empty katastralni_uzemi, cislo_zakazky or ucel').dialog_end()
		return r.page_end().as_string()

	if id == 'new':
		zaznam = model.Zaznam.create(
			cislo = model.Zaznam.get_fresh_number(),
			vytvoren = datetime.now(),
			katastralni_uzemi = ku,
			cislo_zakazky = cz,
			zadavatel = uzivatel.login,
			poznamka = poznamka,
			platny = True
		)
	else:
		zaznam = model.Zaznam.select().where(model.Zaznam.id == int(id)).get()
		if not zaznam.is_authorized(uzivatel):
			raise Exception('Unauthorized')
		zaznam.katastralni_uzemi = ku
		zaznam.cislo_zakazky = cz
		zaznam.poznamka = poznamka
	
	zaznam.save()
	zaznam.set_ucel_ids(ucely)
	
	r = render.GuiRenderer()
	r.page_begin(R('titulek'))
	render_menu(r, uzivatel)

	r.dialog_begin().escape(R('zaznam-byl-vytvoren-s-cislem') % zaznam.format_cislo() if id == 'new' else R('zaznam-byl-ulozen')).dialog_end()
	r.link('/', R('prejit-na-seznam-zaznamu'))
	r.link('/record/%d' % zaznam.id, R('prejit-na-zaznam'))
	
	r.page_end()
	return r.as_string()

@app.route('/list-users')
@auth.requires_auth
@access_log
def list_users():
	uzivatel = logged_user()
	assert is_admin(uzivatel)
	
	r = render.GuiRenderer()
	r.page_begin(R('titulek'))
	render_menu(r, uzivatel)

	r.header(R('seznam-uzivatelu'))
	r.table_begin()
	r.row_begin('header').th(R('login')).th(R('jmeno')).th(R('opravneni')).th(R('povolen')).row_end()
	i = 0
	for u in model.Uzivatel.select().order_by(model.Uzivatel.login):
		r.row_begin('sep' if i else 'first-sep', 'odd' if i % 2 else None)
		r.tdb().escape(u.login).tde()
		r.tdb().escape(u.jmeno).tde()
		r.tdb().escape(R('uzivatel') if u.opravneni == model.Uzivatel.OPRAVNENI_UZIVATEL else R('administrator')).tde()
		r.tdb().escape('' if u.povolen else R('zablokovan')).tde()
		r.row_end()
		i += 1
	r.table_end()

	r.page_end()
	return r.as_string()

@app.route('/history/<int:page>')
@auth.requires_auth
@access_log
def history(page):
	r = render.GuiRenderer()
	r.page_begin(R('titulek'))
	uzivatel = logged_user()
	render_menu(r, uzivatel)
	
	PAGE_LEN = 30
	r.header(R('historie'))
	r.table_begin()
	r.row_begin('header').th(R('cas')).th(R('kdo')).th(R('entita')).th(R('akce')).th(R('popis')).row_end()
	i = 0
	for h in model.Historie.select_authorized(uzivatel).order_by(model.Historie.cas.desc()).paginate(page, PAGE_LEN):
		r.row_begin('sep' if i else 'first-sep', 'odd' if i % 2 else None)
		r.tdb().escape(format(h.cas, '%d.%m.%Y %H:%M:%S')).tde()
		r.tdb().escape(h.uzivatel).tde()
		r.tdb().escape(h.entita).tde()
		r.tdb().escape(R(h.akce)).tde()
		r.tdb().escape(h.popis).tde()
		r.row_end()
		i += 1
	r.table_pages(model.Historie.select_authorized(uzivatel).count(), PAGE_LEN, page, '/history/%d', 5)
	r.table_end()
	
	r.page_end()
	return r.as_string()

@app.route('/export')
@auth.requires_auth
@access_log
def export_list():
	uzivatel = logged_user()
	assert is_admin(uzivatel)
	
	r = render.GuiRenderer()
	r.page_begin(R('titulek'))
	render_menu(r, uzivatel)

	vytvoren_min, vytvoren_max = model.Zaznam.vytvoren_limits()
	r.header(R('export'))
	r.table_begin()
	r.row_begin('header').th(R('rok')).row_end()
	r.row_begin('first-sep').tdb().a('/export/all', R('complete')).tde().row_end()
	if vytvoren_min and vytvoren_max:
		for year in xrange(vytvoren_min.year, vytvoren_max.year + 1):
			r.row_begin('sep').tdb().a('/export/%s' % year, year).tde().row_end()
	r.table_end()
	
	r.page_end()
	return r.as_string()

def export_common(r, query):
	assert is_admin(logged_user())
	r.row(R('cislo'), R('vytvoren'), R('katastralni-uzemi'), R('cislo-zakazky'), R('kdo'), R('poznamka'), R('ucel'))
	for zaznam in query.order_by(model.Zaznam.vytvoren.asc()):
		if not zaznam: continue
		r.item(int(zaznam.cislo) if zaznam.cislo else zaznam.cislo)
		r.item(format(zaznam.vytvoren, '%Y/%m/%d %H:%M:%S') if zaznam.vytvoren else zaznam.vytvoren)
		r.item(zaznam.katastralni_uzemi)
		r.item(zaznam.cislo_zakazky)
		r.item(zaznam.zadavatel)
		r.item(zaznam.poznamka)
		#r.item(zaznam.platny)
		r.item(', '.join(uz.ucel.nazev for uz in model.UcelZaznamu.select().where(model.UcelZaznamu.zaznam == zaznam)))
		r.end()

@app.route('/export/all')
@auth.requires_auth
@access_log
@export('egp_all')
def export_all(r):
	export_common(r, model.Zaznam.select())

@app.route('/export/<int:year>')
@auth.requires_auth
@access_log
@export('egp_year')
def export_year(r, year):
	start = datetime(year, 1, 1)
	finish = datetime(year + 1, 1, 1)
	export_common(r, model.Zaznam.select().where((model.Zaznam.vytvoren > start) & (model.Zaznam.vytvoren < finish)))

@app.route('/favicon.ico')
def favicon():
	return ''

if __name__ == "__main__":
	app.run(debug = True)

