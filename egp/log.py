from flask import request
import config
import logging
from logging.handlers import SysLogHandler
from functools import wraps

logger = logging.getLogger('egp')
handler = SysLogHandler(address = config.SYSLOG_ADDRESS)
#formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt = '%Y/%m/%d %H:%M:%S')
formatter = logging.Formatter('egp[%(levelname)s]: %(message)s', datefmt = '%Y/%m/%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler) 
logger.setLevel(logging.DEBUG)

def access_log(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		global logger
		logger.info(u'%s (%s) - %s', request.authorization.username, request.remote_addr, request.path)
		try:
			return f(*args, **kwargs)
		except BaseException as e:
			logger.error(e)
			raise
	return decorated

