import authdigest
from functools import wraps
from flask import request
from model import Uzivatel

class DB(object):
	def get(self, login):
		try:
			u = Uzivatel.by_login(login)
		except:
			return None
		return u.heslo if u.povolen else None

class Authentication(authdigest.RealmDigestDB):
	def newDB(self):
		return DB()

auth = Authentication('Evidence GP')

def hash_passphrase(username, passphrase):
	return auth.alg.hashPassword(username, auth.realm, passphrase)
	
def requires_auth(f):
	@wraps(f)
	def decorated(*args, **kwargs):
		if not auth.isAuthenticated(request):
			return auth.challenge()
		return f(*args, **kwargs)
	return decorated

