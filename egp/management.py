#!/usr/bin/env python
# coding=UTF-8

import sys
import readline

def input(prompt = None, empty = False):
	while True:
		i = raw_input('%s> ' % (prompt if prompt else '')).decode(sys.stdin.encoding)
		if not empty and not i: continue
		return i
	
import config

if __name__ == '__main__':
	if len(sys.argv) == 1:
		print u'Seznam databází:'
		for db_id in config.DATABASES: print('* %s' % db_id)
		while True:
			db_id = input(u'Vyber databazi')
			if db_id in config.DATABASES: break
			print u'Neznámá databáze: %s' % db_id
	else:
		db_id = sys.argv[1]
	config.set_db(db_id)

from model import *
import auth
from os import chmod
from functools import wraps

ucel_list = {
	'egp': [
		(u'Rozdělení pozemku', False),
		(u'Změnu hranic pozemku', False),
		(u'Vyznačení obvodu budovy', False),
		(u'Změnu obvodu budovy', False),
		(u'Vymezení rozsahu věcného břemene k části pozemku', False),
		(u'Doplnění souboru geodetických informací o pozemek dosud evidovaný ve zjednodušené evidenci', False),
		(u'Průběh vytyčené hranice pozemků', False),
		(u'Průběh vlastníky upřesněné hranice pozemků', False),
		(u'Opravu geometrického a polohového určení nemovitosti', False),
		(u'Upřesnění údajů o parcele podle přídělového řízení', False),
		(u'Určení hranic pozemků při pozemkových úpravách', False),
		(u'Změnu hranice katastrálního území', False),
		(u'Vyznačení vodního díla v katastru', False),
		(u'Změnu obvodu vodního díla v katastru', False),
		(u'Průběh hranice určené soudem', False),
		(u'Vytyčení hranic pozemků (prosté)', False),
		(u'Kopie geometrického plánu', False),
		(u'Tisk geometrického plánu (papírová forma)', True),
	],
	'ing': [
		(u'Inženýrská geodezie', False),
	]
}

def dbtransact(f):
	@wraps(f)
	def decorator(*args, **kwargs):
		with db.transaction():
			return f(*args, **kwargs)
	return decorator

def logerror(f):
	@wraps(f)
	def decorator(*args, **kwargs):
		try:
			return f(*args, **kwargs)
		except BaseException, e:
			from log import logger
			logger.error('Error in %s: %s' % (f.__name__, repr(e)))
			raise
	return decorator

class Management(object):
	def execute(self, args):
		cmd = args.pop(0)
		if hasattr(self, cmd):
			try:
				try:
					getattr(self, cmd)(*args)
				except BaseException, e:
					print 'Error:', repr(e)
			except:
				print 'Unknown error'
		else:
			print 'Unknown command: %s' % cmd
	
	def loop(self):
		while True:
			try:
				cmd = input()
			except EOFError:
				break
			except KeyboardInterrupt:
				break
			args = cmd.strip().split(' ')
			if args[0] in [ 'quit', 'exit' ]: break
			self.execute(args)
		print 'Quitting'
	
	def help(self):
		u"- zobrazí nápovědu"
		print
		print ' Management console '.center(50, '=')
		import types
		for name in sorted(dir(self)):
			o = getattr(self, name)
			if type(o) != types.MethodType: continue
			if not o.__doc__: continue
			print name, o.__doc__
		print u'quit, exit - ukončí sezení'
		print '-' * 50
		print
	
	@dbtransact
	def listusers(self):
		u"- vypíše seznam všech uživatelů"
		for u in Uzivatel.select().order_by(Uzivatel.login, True):
			print u.login.ljust(10), u.jmeno.ljust(20),\
				u'[administrátor]' if u.opravneni == Uzivatel.OPRAVNENI_ADMINISTRATOR else '',\
				'' if u.povolen else u'[zablokován]'

	@dbtransact
	def adduser(self):
		u"- vytvoří nového uživatele"
		name = input('Cele jmeno uzivatele')
		login = input('Login').strip()
		passphrase = input('Heslo')
		if Uzivatel.select().where(Uzivatel.login == login).exists():
			raise Exception('Already exists')
		u = Uzivatel.create(jmeno = name, login = login, heslo = '', povolen = True, opravneni = Uzivatel.OPRAVNENI_UZIVATEL)
		u.heslo = auth.hash_passphrase(login, passphrase)
		u.save()
		
	@dbtransact
	def disuser(self, login):
		u"<login> - dočasně zablokuje uživatele"
		u = Uzivatel.select().where(Uzivatel.login == login).get()
		u.povolen = False
		u.save()
	
	@dbtransact
	def enauser(self, login):
		u"<login> - odblokuje uživatele"
		u = Uzivatel.select().where(Uzivatel.login == login).get()
		u.povolen = True
		u.save()

	@dbtransact
	def rmuser(self, login):
		u"<login> - permanentně odstraní uživatele"
		Uzivatel.by_login(login).delete_instance()
	
	@dbtransact
	def setpass(self, login):
		u"<login> - nastaví heslo pro uživatele"
		u = Uzivatel.select().where(Uzivatel.login == login).get()
		passphrase = input('Heslo')
		u.heslo = auth.hash_passphrase(u.login, passphrase)
		u.save()
	
	def createdb(self):
		u"- vytvoří prázdnou databázi"
		for e in ENTITIES:
			try:
				with db.transaction():
					e.create_table()
					print 'Table for %s created' % e.__name__
			except:
				print 'Error creating table for %s' % e.__name__
		try:
			ucel_names = [ u.nazev for u in Ucel.select() ]
			if config.DB_ID in ucel_list:
				for name, hilite in ucel_list[config.DB_ID]:
					if name in ucel_names: continue
					with db.transaction():
						Ucel.create(nazev = name, povolen = 1, hilite = hilite)
		except Exception as e:
			print 'Error creating Ucel instances:', e
		#try:
		#	chmod(config.DB_PATH, 0o664)
		#except:
		#	print 'Error setting access mode'
	
	@dbtransact
	def adducel(self):
		u"- vytvoří nový Účel"
		title = input('Nazev')
		ucel = Ucel.create(nazev = title, povolen = True, hilite = False)
		ucel.save()
	
	@dbtransact
	def listucel(self):
		u"- zobrazí seznam všech účelů"
		for ucel in Ucel.select().order_by(Ucel.id):
			print ucel.id, ucel.nazev, '' if ucel.povolen else u'[zablokován]', u'[Zvýrazněn]' if ucel.hilite else ''
	
	@dbtransact
	def disucel(self, id):
		u"<id> - dočasně zablokuje vybraný Účel"
		u = Ucel.select().where(Ucel.id == id).get()
		u.povolen = False
		u.save()
	
	@dbtransact
	def enaucel(self, id):
		u"<id> - odblokuje vybraný Účel"
		u = Ucel.select().where(Ucel.id == id).get()
		u.povolen = True
		u.save()
	
	@dbtransact
	def hiliteucel(self, id):
		u"<id> - přepne barevné zvýraznění vybraného účelu"
		u = Ucel.select().where(Ucel.id == id).get()
		u.hilite = not u.hilite
		u.save()
	
	@dbtransact
	def switchadmin(self, login):
		u"<login> - přepne oprávnění uživatele (uživatel <-> administrátor)"
		u = Uzivatel.by_login(login)
		u.opravneni = Uzivatel.OPRAVNENI_ADMINISTRATOR if u.opravneni == Uzivatel.OPRAVNENI_UZIVATEL else Uzivatel.OPRAVNENI_UZIVATEL
		u.save()
	
	@dbtransact
	def rmzaznam(self, cislo):
		u"<cislo> - permanentně odstraní záznam dle zadaného čísla záznamu"
		z = Zaznam.by_cislo(cislo)
		print z.dump(z.get_att_names())
		print u'Opravdu si přejete odstranit tento záznam? (y/n)',
		if raw_input() == 'y':
			z.delete_instance()
			print u'Záznam byl odstraněn'
		else:
			print u'Záznam nebyl odstraněn'
	
	@logerror
	def backup(self, recipient = None):
		u"[recipient(nepovinný)] - vytvoří kompletní zálohu databáze a odešle ji na nakonfigurovanou nebo zadanou emailovou adresu"
		from subprocess import call
		from os import stat
		import gmail
		from datetime import datetime
		from socket import gethostname
		path = '/tmp/egp_backup'
		retcode = call(['pg_dump', '-U', config.DATABASE('USER'), '-w', '-F', 'p', '-Z', '9', '-f', path, config.DATABASE('NAME')])
		assert retcode == 0
		size = stat(path).st_size
		print 'Backup size: %d bytes' % size
		text = 'Zaloha databaze %s na stroji "%s" ze dne %s' % (config.DB_ID, gethostname(), format(datetime.now(), '%d.%m.%Y, %H:%M'))
		msg = gmail.Message()
		msg.subject(text).text(text)
		msg.attach(path, 'pgsql_backup.gz')
		if not recipient: recipient = config.BACKUP_MAIL
		gmail.send(recipient, msg)
		print 'Backup email sent to %s' % recipient

if __name__ == '__main__':
	m = Management()
	if len(sys.argv) == 1:
		m.help()
		m.loop()
	else:
		m.execute(sys.argv[2:])

