import smtplib
import config
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders

class Message(object):
	def __init__(self):
		self.msg = MIMEMultipart()
	
	def header(self, name, content):
		self.msg[name] = content
		return self
	
	def from_(self, addr):
		return self.header('From', addr)

	def to(self, addr):
		return self.header('To', addr)
	
	def subject(self, subject):
		return self.header('Subject', subject)
	
	def text(self, text):
		self.msg.attach(MIMEText(text))
		return self
	
	def attach(self, path, name):
		part = MIMEBase('application', 'octet-stream')
		with open(path, 'r') as f:
			part.set_payload(f.read())
		Encoders.encode_base64(part)
		part.add_header('Content-Disposition', 'attachment; filename="%s"' % name)
		self.msg.attach(part)
		return self
	
	def __str__(self):
		return str(self.msg)

def send(to, msg):
	user = '%s@gmail.com' % config.GMAIL_USER
	msg.from_(user).to(to)
	s = smtplib.SMTP('smtp.gmail.com', 587)
	s.ehlo()
	s.starttls()
	s.ehlo()
	s.login(user, config.GMAIL_PASS)
	s.sendmail(user, to, str(msg))
	s.quit()

