from peewee import fn, Model, PrimaryKeyField, ForeignKeyField, DecimalField, CharField, DateTimeField, TextField, BooleanField, SqliteDatabase, PostgresqlDatabase
import config
from datetime import datetime
from flask import request

#db = SqliteDatabase(config.DATABASE('PATH'), threadlocals = True)
db = PostgresqlDatabase(config.DATABASE('NAME'), user = config.DATABASE('USER'), password = config.DATABASE('PASS'))
db.connection().set_client_encoding('UTF8')

def get_username():
	try:
		return request.authorization.username
	except:
		return u'<console>'

def log_history(instance, action, description):
	#print 'History', datetime.now(), get_username(), unicode(instance), action, description
	return Historie.create(
		uzivatel = get_username(), 
		cas = datetime.now(),
		entita = unicode(instance),
		akce = action,
		popis = description
	)

def log_history_rel(instance, relcls, org_ids, new_ids):
	org_ids = set(org_ids)
	new_ids = set(new_ids)
	for rem_id in org_ids - new_ids:
		log_history(instance, 'REL_REMOVE', unicode(relcls.select().where(relcls.id == rem_id).get()))
	for add_id in new_ids - org_ids:
		log_history(instance, 'REL_ADD', unicode(relcls.select().where(relcls.id == add_id).get()))

class LogModel(Model):
	def get_saved(self):
		try:
			return type(self).select().where(type(self).id == self.id).get()
		except:
			return None
	
	def get_att_names(self):
		return set(self.__data__.keys()) - set(['id'])

	def get_att_value(self, name):
		return self.__data__[name] if name in self.__data__ else None
	
	def dump(self, attnames):
		return u'\n'.join(u'%s: %s' % (name, unicode(getattr(self, name))) for name in attnames)
	
	def save(self, *args, **kwargs):
		org = self.get_saved()
		ret = super(LogModel, self).save(*args, **kwargs)
		if org:
			for name in org.get_att_names() | self.get_att_names():
				oval = org.get_att_value(name)
				nval = self.get_att_value(name)
				if oval == nval: continue
				description = u'%s: %s => %s' % (name, '' if oval == None else unicode(oval), '' if nval == None else unicode(nval))
				log_history(self, 'CHANGE', description)
		else:
			for name in self.get_att_names():
				description = u'%s: %s' % (name, self.get_att_value(name))
				log_history(self, 'NEW', description)
		return ret

	def delete_instance(self, *args, **kwargs):
		log_history(self, 'REMOVE', '')
		super(LogModel, self).delete_instance(*args, **kwargs)

class Zaznam(LogModel):
	id = PrimaryKeyField()
	cislo = DecimalField()
	vytvoren = DateTimeField()
	katastralni_uzemi = CharField()
	cislo_zakazky = CharField()
	zadavatel = CharField()
	poznamka = TextField()
	platny = BooleanField()
	class Meta:
		database = db
	
	@classmethod
	def get_fresh_number(klass):
		try:
			last = Zaznam.select().order_by(Zaznam.vytvoren.desc(), False).get()
		except:
			return 1
		now = datetime.now()
		if now.year != last.vytvoren.year: return 1
		return last.cislo + 1
	
	def iter_ucel(self):
		for uz in UcelZaznamu.select().where(UcelZaznamu.zaznam == self):
			yield uz.ucel
	
	def set_ucel_ids(self, ucel_ids):
		org_ids = []
		for uz in UcelZaznamu.select().where(UcelZaznamu.zaznam == self):
			org_ids.append(uz.ucel.id)
			uz.delete_instance()
		for uid in ucel_ids:
			UcelZaznamu.create(zaznam = self, ucel = uid)
		if org_ids != ucel_ids:
			log_history_rel(self, Ucel, org_ids, ucel_ids)
	
	def format_cislo(self):
		return '%04d' % self.cislo
	
	def is_authorized(self, uzivatel):
		if uzivatel.opravneni == Uzivatel.OPRAVNENI_ADMINISTRATOR: return True
		return self.zadavatel == uzivatel.login
	
	@classmethod
	def select_authorized(klass, uzivatel):
		if uzivatel.opravneni == Uzivatel.OPRAVNENI_ADMINISTRATOR:
			return klass.select()
		return klass.select().where(Zaznam.zadavatel == uzivatel.login)
	
	@classmethod
	def by_cislo(klass, cislo):
		return klass.select().where(Zaznam.cislo == cislo).get()
	
	def delete_instance(self):
		for uz in UcelZaznamu.select().where(UcelZaznamu.zaznam == self):
			uz.delete_instance()
		super(Zaznam, self).delete_instance()
	
	@classmethod
	def vytvoren_limits(klass):
		return klass.select(fn.Min(klass.vytvoren)).scalar(), klass.select(fn.Max(klass.vytvoren)).scalar()
	
	def __unicode__(self):
		return u'Zaznam(cislo=%s)' % self.format_cislo()

class Ucel(LogModel):
	id = PrimaryKeyField()
	nazev = TextField()
	povolen = BooleanField()
	hilite = BooleanField()
	class Meta:
		database = db
	
	def __unicode__(self):
		return u'Ucel(%s)' % self.nazev

class UcelZaznamu(Model):
	ucel = ForeignKeyField(Ucel)
	zaznam = ForeignKeyField(Zaznam)
	class Meta:
		database = db

class Uzivatel(LogModel):
	OPRAVNENI_UZIVATEL = 1
	OPRAVNENI_ADMINISTRATOR = 2
	
	login = CharField()
	jmeno = CharField()
	heslo = CharField()
	povolen = BooleanField()
	opravneni = DecimalField()
	class Meta:
		database = db
	
	@classmethod
	def by_login(klass, login):
		return klass.select().where(Uzivatel.login == login).get()
	
	def get_att_names(self):
		return super(Uzivatel, self).get_att_names() - set(['heslo'])
	
	def __unicode__(self):
		return u'Uzivatel(%s)' % self.jmeno

class Historie(Model):
	id = PrimaryKeyField()
	uzivatel = CharField()
	cas = DateTimeField()
	entita = CharField()
	akce = CharField()
	popis = TextField()
	class Meta:
		database = db
	
	@classmethod
	def select_authorized(klass, uzivatel):
		if uzivatel.opravneni == Uzivatel.OPRAVNENI_ADMINISTRATOR:
			return klass.select()
		return klass.select().where(Historie.uzivatel == uzivatel.login)

ENTITIES = [ Zaznam, Ucel, UcelZaznamu, Uzivatel, Historie ]

